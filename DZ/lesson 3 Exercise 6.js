/*Удалить из массива все столбцы которые
 не имеют ни одного нулевого элемента и
 сумма которых положительна - оформить в виде функции*/

function c(a){
    let max=0;

    for (let i=0;i<a.length-1;i++){
       for (let j=0;j<a[0].length;j++){

            max = a[i][j]+a[i+1][j];
            if (max >= 1){
                 a[i].splice(j, 1);
                 a[i+1].splice(j, 1);
             }
         max = 0; 
        }
    }
return a;
}
var a1= [[8,-3,0,3,4,-6,7,-12,1,0,-1],[1,-2,3,-4,0,6,7,4,-5,-0]];

console.table(a1);
console.table(c(a1));