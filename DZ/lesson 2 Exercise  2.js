/*
Переменная содержит в себе число.
Написать скрипт который посчитает факториал этого числа.
*/
function factorial (n) 
{
    var i, product =1;
    for (i=2; i <=n; i++)
    {
        product *=i;
    }
    return product;
}
factorial(5)