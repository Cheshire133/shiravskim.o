/*
Переменная хранит в себе значение от 0 до 9. 
Написать скрипт который будет выводить слово “один”,
 если переменная хранит значение 1. Выводить слово “два” - если переменная хранит значение 2,
 и т.д. для всех цифр от 0 до 9. Реализовать двумя способами.
*/

 var a = 9;
if (a === 0) {
  alert("Ноль");
} else if (a === 1) {
  alert("Один");
} else if (a === 2) {
  alert("Два");
} else if (a === 3) {
  alert("Три");
} else if (a === 4) {
  alert("Четыре");
} else if (a === 5) {
  alert("Пять");
} else if (a === 6) {
  alert("Шесть");
} else if (a === 7) {
  alert("Семь");
} else if (a === 8) {
  alert("Восемь");
} else if (a === 9) {
  alert("Девять");
} else {
  alert("Ошибка");
}

//Cпособ второй
var a = 5;
switch (a)  {
case 0:
     alert("Ноль");
     break;
case 1:
    alert("Один");
    break;
case 2:
    alert("Два");
    break;
case 3:
     alert("Три");
     break;
case 4:
    alert("Четыре");
    break;
case 5:
    alert("Пять");
    break;
case 6:
    alert("Шесть");
    break;
case 7:
    alert("Семь");
    break;
case 8:
    alert("Восемь");
    break;
case 9:
    alert("Девять");
    break;
default:
  alert("Ошибка");
}