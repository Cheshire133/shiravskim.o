/*
Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB),
 Вторая переменная хранит в себе целое число. 
В зависимости от того какая единица измерения написать скрипт,
 который выводит количество байт.
 Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024
 единиц более меньшего измерения.
*/
var Byte=1,KB=2,MB=3,GB=4;
var a=MB;
var b=8;
if(a===Byte)
    alert(a);
else if(a===KB)
{
    b=b*1024;
    alert(b);
}
else if(a===MB)
{
    b=b*1024*1024;
    alert(b);
}
else if(a===GB)
{
    b=b*1024*1024*1024;
    alert(b);
}