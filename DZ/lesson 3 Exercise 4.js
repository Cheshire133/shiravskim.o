/*Написать функцию, которая умножает две матрицы*/

function c(a, b) {
    let d = [];
    for (let i = 0; i < a.length; i++) {
        d[i] = [];
        for (let j = 0; j < b[0].length; j++) {
            let temp = 0;
            for (let k = 0; k < a[0].length; k++) {
                temp += a[i][k] * b[k][j];
            }
            d[i][j] = temp;
        }
    }
    return d;
}
var a1 = [[1,2],[3,4]]
var b1 = [[5,6],[7,8]]
var c3 = c(a1, b1)
console.table(c3)